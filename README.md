Calibration of the SKIROC2cms for the HGCAL test beams
===

General information about the main test beam chip: the SKIROC2-CMS

* [Overview of the SKIROC2cms ASIC for HGCAL test beams](https://indico.cern.ch/event/609269/contributions/2456363/attachments/1406319/2149796/SC_SK2CMSoverview_20170202_v2.pdf) by @scallier

See more in the repo wiki.

Gain Calibration for October beam test(and injection runs)
