from common_toa_calib import *

indir = "/eos/user/a/alobanov/HGCAL/testbeam/CERN2018/october/data/ntuples/v9/TOA/hdf_files/"

min_run = 975
max_run = 999

fnames = glob.glob(indir + 'ntuple_*_TOA.h5')
print("Found %i files" % len(fnames))

for fname in sorted(fnames):
    #print fname

    run = int(fname[fname.find('ntuple_')+7:fname.find('_TOA')])
    #if run > 263: break

    if run < min_run: continue
    #if (max_run > 0) and (run > max_run): break
    if run > max_run > 0: continue


    prefix = '_run_%i' %run

    df = get_toa_run_df(fname)
    if type(df) == int: continue

    #print df.info()

    df_toa_ranges = get_toa_ranges(df)
    df_toa_ranges.to_csv('calib_oct2018/TOA_calib_ranges' + prefix + '.txt')

    #break
