import os, glob, sys
import pandas as pd
import numpy as np

from scipy.optimize import curve_fit

def toa_func(x, a, b, c, d):
    return a*x + b + c / (x - d)

def tw_func(x, a, b, c, d):
    return a*x + b + c / (x - d)

def get_toa_run_df(fname, min_events = 3000):

    run = int(fname[fname.find('ntuple_')+7:fname.find('_TOA')])

    df = pd.read_hdf(fname, key='/table')
    print(os.path.basename(fname), len(df))

    # Reset event index
    df.reset_index(inplace = True)

    ## Select large runs
    if df.event.max() < min_events: return 0

    ## Select rechits
    #sel = df.rechit_energy > 10
    sel = df.rechit_toaRise > 10
    #sel &= df.rechit_Tot > 10

    df = df[sel]

    ## Set run, global and local event number
    df['run'] = run
    df['loc_event'] = df['event']
    df['event'] = df.run * 1e6 + df.event

    df['rechit_chan_id'] = df.rechit_channel + df.rechit_chip_id*64
    df['rechit_layer'] = df.rechit_chip_id//4

    ## Set event as index
    df.set_index('event', inplace = True)

    ## Convert unsigned to signed ints
    dtypes = {}
    for key,dtype in df.dtypes.iteritems():
        dtypes[key] = str(dtype).replace('uint','int')
    df = df.astype(dtypes, copy = False)

    return df

def get_toa_df(indir, runlist = None, min_run = -1, max_run = -1, min_events = 3000):

    fnames = glob.glob(indir + 'ntuple_*_TOA.h5')
    print("Found %i files" % len(fnames))

    dfs = []
    for fname in sorted(fnames):
        #print fname

        run = int(fname[fname.find('ntuple_')+7:fname.find('_TOA')])
        #if run > 263: break

        if run < min_run: continue
        #if (max_run > 0) and (run > max_run): break
        if run > max_run > 0: continue

        df = pd.read_hdf(fname, key='/table')
        print(os.path.basename(fname), len(df))

        # Reset event index
        df.reset_index(inplace = True)

        ## Select large runs
        if df.event.max() < min_events: continue

        ## Select rechits
        #sel = df.rechit_energy > 10
        sel = df.rechit_toaRise > 10
        #sel &= df.rechit_Tot > 10

        df = df[sel]

        ## Set run, global and local event number
        df['run'] = run
        df['loc_event'] = df['event']
        df['event'] = df.run * 1e6 + df.event

        df['rechit_chan_id'] = df.rechit_channel + df.rechit_chip_id*64
        df['rechit_layer'] = df.rechit_chip_id//4

        ## Set event as index
        df.set_index('event', inplace = True)

        ## Convert unsigned to signed ints
        dtypes = {}
        for key,dtype in df.dtypes.iteritems():
            dtypes[key] = str(dtype).replace('uint','int')
        df = df.astype(dtypes, copy = False)

        dfs.append(df)

        print('Added run %i' %run)

    print('Found %i files' %len(dfs))

    if len(dfs) > 0:
        df = pd.concat(dfs)

        return df
    else:
        return 0

def get_toa_ranges(df, min_toa_range = 1400):

    ## Selection for TOA ranges
    sel = df.rechit_toaRise > 4
    sel &= df.rechit_toaFall > 4

    # sel &= df.run < 243
    # sel &= df.rechit_Tot < 500
    # sel &= df.rechit_Tot > 300

    df_by_chan_id = df[sel].groupby('rechit_chan_id')

    toaRise_min = df_by_chan_id['rechit_toaRise'].min()
    toaRise_max = df_by_chan_id['rechit_toaRise'].max()

    toaFall_min = df_by_chan_id['rechit_toaFall'].min()
    toaFall_max = df_by_chan_id['rechit_toaFall'].max()

    ## Create DF to store
    df_toa_ranges = pd.DataFrame([toaRise_min,toaRise_max,toaFall_min,toaFall_max]).T
    df_toa_ranges.columns = ['toaRise_min','toaRise_max','toaFall_min','toaFall_max']
    df_toa_ranges.reset_index(inplace=True)
    df_toa_ranges['rechit_chip_id'] = df_toa_ranges.rechit_chan_id//64
    df_toa_ranges['rechit_chan_id'] -= ((df_toa_ranges['rechit_chip_id'])*64)
    df_toa_ranges.set_index(['rechit_chip_id','rechit_chan_id'], inplace=True)
    df_toa_ranges.index.names = ['chip_id','chan_id']

    ## Select channels with large enough range
    sel = (df_toa_ranges.toaFall_max - df_toa_ranges.toaFall_min) > min_toa_range
    df_toa_ranges = df_toa_ranges[sel]

    return df_toa_ranges

def norm_toa_ranges_slow(df, df_toa_ranges):

    df_toa_ranges = df_toa_ranges.reset_index()

    df_toa_ranges.chip_id*64 + df_toa_ranges.chan_id
    df_toa_ranges['rechit_chan_id'] = df_toa_ranges.chip_id*64 + df_toa_ranges.chan_id
    df_toa_ranges.set_index('rechit_chan_id', inplace = True)

    df['toaRise_min'] = df['rechit_chan_id'].map(df_toa_ranges.toaRise_min)
    df['toaRise_max'] = df['rechit_chan_id'].map(df_toa_ranges.toaRise_max)

    df['rechit_toaRise_norm'] = df['rechit_toaRise'] - df.toaRise_min
    df['rechit_toaRise_norm'] /=  (df.toaRise_max-df.toaRise_min)

    df['toaFall_min'] = df['rechit_chan_id'].map(df_toa_ranges.toaFall_min)
    df['toaFall_max'] = df['rechit_chan_id'].map(df_toa_ranges.toaFall_max)

    df['rechit_toaFall_norm'] = df['rechit_toaFall'] - df.toaFall_min
    df['rechit_toaFall_norm'] /=  (df.toaFall_max-df.toaFall_min)

    ## drop not needed columns
    df.drop(['toaRise_min','toaRise_max','toaFall_min','toaFall_max'], axis=1, inplace=True)

    return 1

def norm_toa_ranges(df, df_toa_ranges):

#     for chip in list(set(df.rechit_chip_id)):
#         for chan in list(set(df.rechit_channel)):

#             sel_chan = df.rechit_chip_id == chip
#             sel_chan &= df.rechit_channel == chan

#             toaRise_min = df_toa_ranges.loc[(chip,chan),'toaRise_min']
#             toaRise_max = df_toa_ranges.loc[(chip,chan),'toaRise_max']

#             df.loc[sel_chan,'rechit_toaRise_norm'] = (df.loc[sel_chan,'rechit_toaRise'] - toaRise_min)/(toaRise_max-toaRise_min)

#             toaFall_min = df_toa_ranges.loc[(chip,chan),'toaFall_min']
#             toaFall_max = df_toa_ranges.loc[(chip,chan),'toaFall_max']

#             df.loc[sel_chan,'rechit_toaFall_norm'] = (df.loc[sel_chan,'rechit_toaFall'] - toaFall_min)/(toaFall_max-toaFall_min)


    chips = df.rechit_chip_id.unique()
    chans = df.rechit_channel.unique()

    d_ranges = df_toa_ranges.to_dict()

    for chip in chips:
        sel_chip = df.rechit_chip_id == chip

        for chan in chans:

            if (chip,chan) not in d_ranges['toaRise_min']: continue

            toaRise_min = d_ranges['toaRise_min'][(chip,chan)]
            toaRise_max = d_ranges['toaRise_max'][(chip,chan)]

            toaFall_min = d_ranges['toaFall_min'][(chip,chan)]
            toaFall_max = d_ranges['toaFall_max'][(chip,chan)]

            sel_chan = (sel_chip) & (df.rechit_channel == chan)

            df.loc[sel_chan,'rechit_toaRise_norm'] = (df.loc[sel_chan,'rechit_toaRise'] - toaRise_min)/(toaRise_max-toaRise_min)
            df.loc[sel_chan,'rechit_toaFall_norm'] = (df.loc[sel_chan,'rechit_toaFall'] - toaFall_min)/(toaFall_max-toaFall_min)
    return 1

'''
def apply_toa_calib(df, df_toa_calib_chan, df_toa_calib_chip = None, toa_type = 'Rise'):

    toa_colname = 'toa_' + toa_type.lower() + '_time'
    df[toa_colname] = 10

    for chip in list(set(df.rechit_chip_id)):
        for chan in list(set(df.rechit_channel)):
            #print 'Hey', chip, chan

            params = 0
            ## Check whether calibration for this channel exists
            if (chip,chan) in df_toa_calib_chan.index:
                params = df_toa_calib_chan.loc[chip, chan].tolist()
            ## Check chip wise calibration
            elif df_toa_calib_chip is not None:
                if chip in df_toa_calib_chip.index:
                    params = df_toa_calib_chan.loc[chip].tolist()

            sel_chan = df.rechit_chip_id == chip
            sel_chan &= df.rechit_channel == chan

            if params == 0:
                df.loc[sel_chan,toa_colname] = 0
            else:
                df.loc[sel_chan,toa_colname] = df.loc[sel_chan,'rechit_toa' + toa_type + '_norm'].apply(lambda x: 25-toa_func(x,*params))

    return 1
'''

def apply_toa_calib(df, df_toa_calib_chan, df_toa_calib_chip = None, toa_type = 'Rise'):

    toa_colname = 'toa_' + toa_type.lower() + '_time'
    df[toa_colname] = 0

    chips = sorted(df.rechit_chip_id.unique())
    chans = sorted(df.rechit_channel.unique())

    d_toa_calib_chan = df_toa_calib_chan.to_dict('index')
    d_toa_calib_chip = df_toa_calib_chip.to_dict('index')

    print('Total %i chips' %len(chips))

    for i_chip, chip in enumerate(chips):
        sel_chip = df.rechit_chip_id == chip
        
        print(i_chip, )

        for chan in chans:

            params = 0
            ## Check whether calibration for this channel exists
            if (chip,chan) in d_toa_calib_chan:
                params = d_toa_calib_chan[(chip, chan)]#.values()
            ## Check chip wise calibration
            elif df_toa_calib_chip is not None:
                if chip in d_toa_calib_chip:
                    params = d_toa_calib_chip[chip]#.values()

            sel_chan = sel_chip & (df.rechit_channel == chan)
            
            if params == 0:
                df.loc[sel_chan,toa_colname] = 0
            else:
                params = [params['a'],params['b'],params['c'],params['d']]
                df.loc[sel_chan,toa_colname] = df.loc[sel_chan,'rechit_toa' + toa_type + '_norm'].apply(lambda x: 25-toa_func(x,*params))

    print('done')

    return 1

def fit_toa_params(vals, binned = True):

    if binned:
        ## create histo
        hist, bin_edges = np.histogram(vals, density=True, bins = np.linspace(0,1,1000))

        ## calculate cumulative sums
        cumvals = np.cumsum(hist)
        cumvals /= cumvals.max()
        y = 25 * (1 - cumvals)
        x = (bin_edges[1:] + bin_edges[:-1])/2.

    else:
        x = vals.sort_values()
        y = np.linspace(25,0,len(vals))

    ## fit
    fit_params, pcov = curve_fit(toa_func, x,y,
                                p0 = [-15.,   27.,   3.,   1.3],
                                bounds = ([-20,   10,   1,   1.],
                                          [-5,   40,   40,   2.]))

    return fit_params.tolist()

def get_toa_calib_perchan(df):
    ## per channel
    chan_toa_rise_calib = {}
    chan_toa_fall_calib = {}

    base_sel = df.rechit_toaRise > 4
    base_sel &= df.rechit_toaFall > 4

    for chip in list(set(df.rechit_chip_id)):
        for chan in list(set(df.rechit_channel)):

            sel_chan = base_sel & (df.rechit_chip_id == chip)
            sel_chan &= (df.rechit_channel == chan)

            nhits = sum(sel_chan)
            if nhits < 1000: continue

            fit_params = fit_toa_params(df[sel_chan].rechit_toaRise_norm)
            chan_toa_rise_calib[(chip,chan)] = fit_params

            fit_params = fit_toa_params(df[sel_chan].rechit_toaFall_norm)
            chan_toa_fall_calib[(chip,chan)] = fit_params

    calibs = []
    for calib_dict in [chan_toa_rise_calib, chan_toa_fall_calib]:

        calib_dict = pd.DataFrame(calib_dict).T
        calib_dict.columns = ['a','b','c','d']
        calib_dict.index.names = ['chip_id','chan_id']

        calibs.append(calib_dict)


    for calib_dict in [chip_toa_rise_calib, chip_toa_fall_calib]:
        calib_dict = pd.DataFrame(calib_dict).T
        calib_dict.columns = ['a','b','c','d']
        calib_dict.index.name = 'chip_id'

        calibs.append(calib_dict)


    return calibs

def get_toa_calib(df):

    ## per chip
    chip_toa_rise_calib = {}
    chip_toa_fall_calib = {}

    ## per channel
    chan_toa_rise_calib = {}
    chan_toa_fall_calib = {}

    base_sel = df.rechit_toaRise > 4
    base_sel &= df.rechit_toaFall > 4

    for chip in list(set(df.rechit_chip_id)):

        sel_chan = base_sel & (df.rechit_chip_id == chip)

        fit_params = fit_toa_params(df[sel_chan].rechit_toaRise_norm)
        chip_toa_rise_calib[chip] = fit_params

        fit_params = fit_toa_params(df[sel_chan].rechit_toaFall_norm)
        chip_toa_fall_calib[chip] = fit_params

        for chan in list(set(df.rechit_channel)):

            sel_chan = base_sel & (df.rechit_chip_id == chip)
            sel_chan &= (df.rechit_channel == chan)

            nhits = sum(sel_chan)
            if nhits < 1000: continue

            fit_params = fit_toa_params(df[sel_chan].rechit_toaRise_norm)
            chan_toa_rise_calib[(chip,chan)] = fit_params

            fit_params = fit_toa_params(df[sel_chan].rechit_toaFall_norm)
            chan_toa_fall_calib[(chip,chan)] = fit_params

            #print chip, chan, nhits,
            #print 'TOA fit', fit_params
            #print toa_func(0,*fit_params), toa_func(1,*fit_params)

    calibs = []
    for calib_dict in [chan_toa_rise_calib, chan_toa_fall_calib]:

        calib_dict = pd.DataFrame(calib_dict).T
        calib_dict.columns = ['a','b','c','d']
        calib_dict.index.names = ['chip_id','chan_id']

        calibs.append(calib_dict)


    for calib_dict in [chip_toa_rise_calib, chip_toa_fall_calib]:
        calib_dict = pd.DataFrame(calib_dict).T
        calib_dict.columns = ['a','b','c','d']
        calib_dict.index.name = 'chip_id'

        calibs.append(calib_dict)


    return calibs
