# Instructions on running the calibration procedure
Step 1:

Make a txt file which contains all the testbeam ntuples you want to calibrate.

(ex: /eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v12/ntuple_1125.root

     /eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v12/ntuple_1126.root
     
     /eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v12/ntuple_1127.root)
     
     
Step 2:

Write the txt file made in Step 1 into data_input.txt

(ex: less run258_722_v12.txt > data_input.txt)


Step 3:

Create root with TProfile in HGLG/LGTOT for each Board, default filename = outpath/TPro.root.

(ex: ./Gain_Study -t TPro_pion_ele_config1_run258_722_v12.root)


Step 4:

Use filename.root as input file for fitting( Should be an output file of ./Gain_Study -t)

./Gain_Study -f Module_TProfile/TPro_pion_ele_config1_run258_722_v12.root